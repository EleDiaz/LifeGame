/**
 * life - Cell.java 15/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.models;

/**
 * TODO: Commenta algo
 *
 */
public abstract class Cell {

    /**
     * Update a cell state to next generation
     * @param universe Universe with all friends cells
     * @param cellState current state of cell to update
     * @param x pos in x of cell
     * @param y pos in y of cell
     */
    public abstract CellState step(Universe universe, CellState cellState, int x, int y);

    public int countAliveNeighbours(Universe universe, int x, int y) {
        int count = 0;
        //count += universe.getCell(x, y) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x + 1, y) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x - 1, y) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x, y - 1) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x, y + 1) == CellState.CELL_ALIVE ? 1 : 0;

        count += universe.getCell(x + 1, y + 1) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x + 1, y - 1) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x - 1, y + 1) == CellState.CELL_ALIVE ? 1 : 0;
        count += universe.getCell(x - 1, y - 1) == CellState.CELL_ALIVE ? 1 : 0;
        return count;
    }
}
