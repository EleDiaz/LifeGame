/**
 * BallsGame - Canvas.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.views;

import es.ull.pai.life.reactive.Behavior;
import es.ull.pai.life.reactive.Event;
import es.ull.pai.life.utils.Tuple;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.function.Consumer;

/**
 * Canvas to paint into screen its trigger paint Event
 */
public class Canvas extends JPanel {
    private final Dimension PANEL_SIZE = new Dimension(600, 600);
    private Event<Graphics2D> paint = new Event<>();
    private Event<MouseEvent> mouse = new Event<>();
    private Event<Tuple<Integer>> click = new Event<>();
    private Behavior<Tuple<Integer>> dimension = new Behavior<>(new Tuple<>(0, 0));

    public Canvas() {
        setPreferredSize(PANEL_SIZE);
        addMouseMotionListener(bindMousePos());
        addMouseListener(bindMousePress());
        addComponentListener(bindSize());
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.translate(0, getHeight());
        graphics2D.scale(1.0, -1.0);
        graphics2D.setColor(Color.GRAY);
        graphics2D.fillRect(0,0, getWidth(), getHeight());
        paint.send(graphics2D);
    }

    /**
     * Let automatically save affine transformation to end of application retrieve again
     * @param graphics2D
     */
    public static void with(Graphics2D graphics2D, Consumer<Graphics2D> consumer) {
        AffineTransform mat = graphics2D.getTransform();
        consumer.accept(graphics2D);
        graphics2D.setTransform(mat);
    }

    /**
     * Bind Size callback with event
     * @return binder
     */
    private ComponentAdapter bindSize() {
        return new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent componentEvent) {
                super.componentResized(componentEvent);
                dimension.updates().send(new Tuple<>(getWidth()
                                                    , getHeight()));
                repaint();
            }
        };
    }

    /**
     * Bind mouse position with event
     * @return binder
     */
    private MouseMotionAdapter bindMousePos() {
        return new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                super.mouseMoved(mouseEvent);
                getMouse().send(mouseEvent);
            }
        };
    }

    /**
     * Bind mouse click with event
     * @return binder
     */
    private MouseAdapter bindMousePress() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                getClick().send(new Tuple<>(mouseEvent.getX(), getHeight() - mouseEvent.getY()));
                repaint();
            }
        };
    }

    /**
     * Get graphics event, with it is possible add new dispatch actions
     */
    public Event<Graphics2D> getPaint() {
        return paint;
    }

    /**
     * Get mouse event
     */
    public Event<MouseEvent> getMouse() {
        return mouse;
    }

    /**
     * Set mouse event
     * @param mouse
     * @return
     */
    public Canvas setMouse(Event<MouseEvent> mouse) {
        this.mouse = mouse;
        return this;
    }

    /**
     * Get Dimension Event
     */
    public Behavior<Tuple<Integer>> getDimension() {
        return dimension;
    }

    /**
     * Se dimension
     * @param dimension
     * @return
     */
    public Canvas setDimension(Behavior<Tuple<Integer>> dimension) {
        this.dimension = dimension;
        return this;
    }

    /**
     * Get click mouse event
     */
    public Event<Tuple<Integer>> getClick() {
        return click;
    }

    /**
     * Set click mouse event
     * @param click
     * @return
     */
    public Canvas setClick(Event<Tuple<Integer>> click) {
        this.click = click;
        return this;
    }
}
