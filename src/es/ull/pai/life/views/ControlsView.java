/**
 * life - ControlsView.java 16/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.views;

import es.ull.pai.life.Resources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;

/**
 * TODO: Commenta algo
 *
 */
public class ControlsView extends JPanel {
    private static final String CLEAR_STR = "Clear";
    private static final String STOP_STR = "Stop";
    private static final String START_STR = "Start";
    private static final String STEP_STR = "Step";
    private JButton startBtn = new JButton(START_STR);
    private JButton stopBtn  = new JButton(STOP_STR);
    private JButton stepBtn  = new JButton(STEP_STR);
    private JButton clearBtn = new JButton(CLEAR_STR);
    private JSlider speedSld = new JSlider();
    private JComboBox selectMatrix = new JComboBox();

    public ControlsView() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

        speedSld.setPaintLabels(true);
        Hashtable<Integer, JLabel> table = new Hashtable<Integer, JLabel>();
        table.put(speedSld.getMinimum(), new JLabel("" + speedSld.getMinimum()));
        table.put(speedSld.getMaximum(), new JLabel("" + speedSld.getMaximum()));
        speedSld.setLabelTable (table);

        add(startBtn);
        add(stopBtn);
        add(stepBtn);
        add(clearBtn);
        add(speedSld);
        add(selectMatrix);
        setUpInfo();
    }

    /**
     * Make a info button.
     */
    public void setUpInfo() {
        final String INFO_TITLE = "Information";
        final String INFO = "Práctica 1101\n"
                + "Descripción: Juego de la vida, el de Conway\n"
                + "Universidad de La Laguna para la asignatura \"PAI\"\n\n"
                + "Made with Java by Eleazar Díaz Delgado\n"
                + "Contact: https://github.com/EleDiaz";
        final int SIZE_ICON = 24;

        JButton button = new JButton(Resources.getInstance().getInfoIcon());
        button.setPreferredSize(new Dimension(SIZE_ICON, SIZE_ICON));
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                JOptionPane.showMessageDialog(null, INFO, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
            }
        });
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.SOUTH);
    }

    /**
     *
     */
    public JButton getClearBtn() {
        return this.clearBtn;
    }

    /**
     *
     */
    public JComboBox getSelectMatrix() {
        return this.selectMatrix;
    }

    /**
     *
     */
    public JSlider getSpeedSld() {
        return this.speedSld;
    }

    /**
     *
     */
    public JButton getStartBtn() {
        return startBtn;
    }

    /**
     *
     */
    public JButton getStopBtn() {
        return stopBtn;
    }

    /**
     *
     */
    public JButton getStepBtn() {
        return stepBtn;
    }
}
